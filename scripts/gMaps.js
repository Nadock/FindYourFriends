function initialise()
{
	if (navigator.geolocation)
	{
     		navigator.geolocation.getCurrentPosition(function (position)
     		{
         		createBackgroundMap(position.coords.latitude, position.coords.longitude);
     		});
	}
	else
	{
		createBackgroundMap(-34, 138)
	}
}

function createBackgroundMap(latitude, longitude)
{
	var mapOptions =
	{
		center: new google.maps.LatLng(latitude, longitude),
		zoom: 11,
		disableDefaultUI: true,
	};
	
        var map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
}
