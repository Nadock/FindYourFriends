$(document).ready(function ()
{
	$("#signupForm").submit(function (event)
	{
		var validForm = 1;
		if ( !isValidEmailAddress($("#signupEmail").val()) )
		{
			$("#signUpEmailError").text("Please enter a valid email.");
			validForm = 0;
		}
		else
		{
			$("#signUpEmailError").text("");
		}

		var passwordValidity = isValidPasswordPair($("#signupPass").val(), $("#signupPassConf").val());
		if (passwordValidity === 1)
		{
			$("#signUpPasswordError").text("Passwords do not match");
			validForm = 0;
		}
		else if (passwordValidity == 2)
		{
			$("#signUpPasswordError").text("Password must be at least 8 characters long.");
			validForm = 0;
		}
		else
		{
			$("#signUpPasswordError").text("");	
		}

		if ( !($("#signupTC").is(":checked")) )
		{
			$("#signUpTermsError").text("Please agree to the terms and conditions.");
			validForm = 0;
		}	
		else
		{
			$("#signUpTermsError").text("");
		}
		if (validForm === 0)
		{
			event.preventDefault();
		};
	})

	$("#loginForm").submit(function (event)
	{
		if( emailOrPassEmpty($("#loginEmail").val(), $("#loginPass").val()) )
		{
			console.log("email || pass empty");
			event.preventDefault();
		}
		else
		{
			console.log("login");
			$("#formContainer").remove();
			$("#mapScreen").remove();
			$(".title").remove();
			enableSearchBar();
		}
	})

	$("#searchBox").on("click", function()
	{
		if ($(this).val() == "Search for a friend...")
		{
			$(this).val("");
		};
	});

	$("#searchBox").focusout(function()
	{
		if ($(this).val() == "")
		{
			$(this).val("Search for a friend...");
		};
	});
});

function isValidEmailAddress(emailAddress)
{
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};

function isValidPasswordPair(password, confirmPassword)
{
	
	if (password != confirmPassword)
	{
		return 1;
	}
	else if (password.length < 8)
	{
		return 2;
	}
	return 0;
}

function emailOrPassEmpty(email, pass)
{
	if (email.length === 0 || pass.length === 0)
	{
		return true;
	}
	return false;
}

function enableSearchBar()
{
	$("#searchBar").css("visibility", "visible");
}

